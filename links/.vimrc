call plug#begin("~/.vim/plugged")
"Plug 'jalvesaq/Nvim-R'
"Plug 'roxma/nvim-yarp'
"Plug 'roxma/vim-hug-neovim-rpc'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdcommenter'
"Plug 'vim-scripts/indentpython.vim'
Plug 'ycm-core/YouCompleteMe'
Plug 'scrooloose/nerdtree'
Plug 'brtastic/vim-jsonviewer'
Plug 'SirVer/ultisnips'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'guns/xterm-color-table.vim'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ervandew/supertab'
Plug 'haya14busa/incsearch.vim'
Plug 'machakann/vim-highlightedyank'
Plug 'suan/vim-instant-markdown'
"Plug 'jeaye/color_coded'
Plug 'ryanoasis/vim-devicons'
Plug 'DehanLUO/nerdtree-project-plugin'
Plug 'flazz/vim-colorschemes'
Plug 'morhetz/gruvbox'
Plug 'dyng/ctrlsf.vim'
"Plug 'octol/vim-cpp-enhanced-highlight'
"Plug 'vim-syntastic/syntastic'
Plug 'preservim/tagbar'
Plug 'puremourning/vimspector'
Plug 'dense-analysis/ale'
Plug 'Raimondi/delimitMate'
"Plug 'python-rope/ropevim'
call plug#end()


colorscheme gruvbox
set bg=dark
let g:gruvbox_contrast_dark='hard'
let mapleader=" "

" basic VIM settings
set number                " show line numbers
set relativenumber
set numberwidth=4         " set linenumber column width
set tabstop=4             " define tab width as two spaces
set softtabstop=4
set shiftwidth=4          " define indent width
set noexpandtab             " insets spaces with pressed tab
set encoding=utf-8        " sets default encoding interpretation
set backspace=indent,eol,start    " enable more free backspace use
set list
set listchars=tab:·\ \|,trail:␠,space:·
set incsearch
set scrolloff=8
let g:pymode_python = 'python3'

" autoread on pull
set autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "Buffer changed!" | echohl None
" NERDTree
map <C-t> :NERDTreeToggle<CR>:TagbarToggle<CR>
let NERDTreeShowHidden=1
let NERDTreeWinSize=51

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<s-tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" indentGuide
let g:indent_guides_auto_colors = 0
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd ctermbg=235 ctermfg=244
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=234 ctermfg=244

" vim indent for c files
autocmd Filetype c,h setlocal tabstop=8
autocmd Filetype c,h setlocal softtabstop=8
autocmd Filetype c,h setlocal shiftwidth=8
autocmd Filetype c,h setlocal noexpandtab

" vim indent for python files
autocmd Filetype python setlocal tabstop=4
autocmd Filetype python setlocal softtabstop=4
autocmd Filetype python setlocal shiftwidth=4
autocmd Filetype python setlocal expandtab
autocmd Filetype pyhton setlocal autoindent

"" curly bracket autocomplete
"inoremap {      {}<Left>
"inoremap {<CR>  {}<Left><CR><CR><Up><Tab>
"inoremap {<Right>     {
"inoremap {}     {}

"" square bracket autocomplete
"inoremap [            []<Left>
"inoremap [<CR>        []<Left><CR><CR><Up><Tab>
"inoremap [<Right>     [
""inoremap [[           [[]]<Left><Left>
"inoremap []           []

"" parentheses autocomplete
"inoremap (      ()<Left>
"inoremap (<CR>  ()<Left><CR><CR><Up><Tab>
"inoremap (<Right>     (
"inoremap ()     ()

"" double quote autocomplete
"inoremap "      ""<Left>
"inoremap ""     ""

"" single quote autocomplete"
"inoremap '      ''<Left>
"inoremap ''     ''

" set highlight colors
highlight Normal guibg=NONE
highlight ExtraWhitespace ctermbg=red
highlight ColorColumn ctermbg=236
"highlight SpaceIndent ctermbg=89
highlight SpecialKey ctermbg=235 ctermfg=239

match ExtraWhitespace /\s\+$/             " highlight trailing whitespace

let &colorcolumn=join(range(81,999),",")  " highlight 80 column limit

" show buffers with F5
:nnoremap <F5> :buffers<CR>:buffer<Space>

"" map interpunction to send code in Nvim-R
"nmap , <Plug>RDSendLine
"nmap . <Plug>RSendFile
"vmap ,e <Plug>RESendSelection

" map fold creation
nmap zn :set foldmethod=indent<CR>

" ctrlfp settings
let g:ctrlsf_default_view_mode = 'compact'
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>
let g:ctrlsf_auto_focus = {
    \ "at": "start",
    \ }
let g:ctrlsf_auto_close = {
    \ "normal" : 1,
    \ "compact": 1
    \}
let g:ctrlsf_default_root = 'project'
let g:ctrlsf_auto_preview = 1
let R_tmpdir = '/tmp/R_tmp_dir'
let g:ctrlsf_search_mode = 'async'

map <leader>g  :YcmCompleter GoToDeclaration<CR>
nmap <leader>yfw <Plug>(YCMFindSymbolInWorkspace)
nmap <leader>yrn <Plug>(YCMRefactorRename)
nmap <leader>yre <Plug>(YCMRefactorExtractVariable)
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_enable_semantic_highlighting=1

" Vimspector
nnoremap <leader>bp :call vimspector#ToggleBreakpoint()<CR>
nnoremap <leader>bl :call vimspector#Launch()<CR>
nnoremap <leader>br :call vimspector#Reset()<CR>
nnoremap <leader>bc :call vimspector#Continue()<CR>
nmap <leader>bk <Plug>VimspectorRestart
nmap <leader>bh <Plug>VimspectorStepOut
nmap <leader>bl <Plug>VimspectorStepInto
nmap <leader>bj <Plug>VimspectorStepOver

" ALE config
let g:ale_linters = {
\   'python': ['ruff', 'mypy'],
\}
