-- UI config
vim.opt.number = true
vim.opt.relativenumber = true

-- searching
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- listchars
vim.opt.listchars:append({
  tab = "· |",
  space = "·",
  trail = "␠",
  eol = "↴",
})
vim.opt.list = true
vim.opt.colorcolumn = { 80, 88, 120 }

-- treesitter folding
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

-- foldtext
function _G.fold_text()
  local line = vim.fn.getline(vim.v.foldstart)
  local folded_line_num = vim.v.foldend - vim.v.foldstart
  local fillcharcount = 88 - string.len(line) - string.len(folded_line_num)
  return line
    .. " "
    .. string.rep("↓", fillcharcount)
    .. " = "
    .. folded_line_num
    .. " lines"
end
vim.o.foldtext = "v:lua.fold_text()"
