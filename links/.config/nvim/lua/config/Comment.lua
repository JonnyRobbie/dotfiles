require("Comment").setup({
  toggler = {
    line = "<Leader>c<Leader>",
    block = "<Leader>cl",
  },
  opleader = {
    line = "<Leader>c<Leader>",
    block = "<Leader>cl",
  },
})
