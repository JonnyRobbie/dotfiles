require("nvim-treesitter.configs").setup({
  ensure_installed = {
    "python",
    "make",
    "json",
    "javascript",
    "html",
    "css",
    "bash",
    "r",
    "rust",
    "lua",
  },
  highlight = { enable = true },
})
