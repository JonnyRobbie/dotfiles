require("ibl").setup({
  indent = { char = "▏" },
  -- scope = { char = "▎",  highlight = { "Function", "Label" }}
  scope = { char = "▎", enabled = true, show_exact_scope = true },
})
