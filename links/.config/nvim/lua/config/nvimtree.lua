require("nvim-tree").setup({
  view = {
    width = 51,
  },
})

vim.keymap.set("n", "<C-t>", ":NvimTreeToggle<CR>")
