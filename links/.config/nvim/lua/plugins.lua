-- packer bootstrap
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath("data")
    .. "/site/pack/packer/start/packer.nvim"
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({
      "git",
      "clone",
      "--depth",
      "1",
      "https://github.com/wbthomason/packer.nvim",
      install_path,
    })
    vim.cmd([[packadd packer.nvim]])
    return true
  end
  return false
end
local packer_bootstrap = ensure_packer()

-- Packer sync after pligins.lua change
-- vim.cmd([[
--   augroup packer_user_config
--     autocmd!
--     autocmd BufWritePost plugins.lua source <afile> | PackerSync
--   augroup end
-- ]])

-- Packer.nvim hints
--     after = string or list,           -- Specifies plugins to load before this plugin. See "sequencing" below
--     config = string or function,      -- Specifies code to run after this plugin is loaded
--     requires = string or list,        -- Specifies plugin dependencies. See "dependencies".
--     ft = string or list,              -- Specifies filetypes which load this plugin.
--     run = string, function, or table, -- Specify operations to be run after successful installs/updates of a plugin
return require("packer").startup(function(use)
  -- Packer can manage itself
  use({ "wbthomason/packer.nvim" })
  -- Colortheme
  use({ "gruvbox-community/gruvbox" })
  -- LSP
  use({ "neovim/nvim-lspconfig" })
  use({ "williamboman/mason.nvim" })
  use({ "williamboman/mason-lspconfig.nvim" })
  -- Linter
  use({ "mfussenegger/nvim-lint" })
  -- Autocompletion
  use({ "hrsh7th/nvim-cmp" })
  use({ "hrsh7th/cmp-nvim-lsp" })
  use({ "hrsh7th/cmp-buffer", after = "nvim-cmp" }) -- buffer auto-completion
  use({ "hrsh7th/cmp-path", after = "nvim-cmp" }) -- path auto-completion
  use({ "hrsh7th/cmp-cmdline", after = "nvim-cmp" }) -- cmdline auto-completion
  -- Snippets
  use({ "L3MON4D3/LuaSnip" })
  use({ "saadparwaiz1/cmp_luasnip" })
  -- Comments
  use({ "numToStr/Comment.nvim" })
  -- Treesitter
  use({
    "nvim-treesitter/nvim-treesitter",
    run = function()
      local ts_update =
        require("nvim-treesitter.install").update({ with_sync = true })
      ts_update()
    end,
  })
  -- Autopairs
  use({ "windwp/nvim-autopairs" })
  -- File tree explorer
  use({ "nvim-tree/nvim-tree.lua" })
  use({ "nvim-tree/nvim-web-devicons" })
  use({ "ThePrimeagen/harpoon", branch = "harpoon2" })
  -- Indent guides
  use({ "lukas-reineke/indent-blankline.nvim" })
  use({ "lukas-reineke/virt-column.nvim" })
  -- Search
  use({
    "nvim-telescope/telescope.nvim",
    -- tag = "0.1.2",
    requires = { { "nvim-lua/plenary.nvim" } },
  })
  -- Yank
  use({ "machakann/vim-highlightedyank" })

  if packer_bootstrap then
    require("packer").sync()
  end
end)
