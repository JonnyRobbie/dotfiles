-- Based on https://martinlwx.github.io/en/config-neovim-from-scratch/
require("options")
require("keymaps")
require("plugins")
require("colorscheme")
require("lsp")
require("config.treesitter")
require("config.nvim-cmp")
require("config.Comment")
require("config.autopairs")
require("config.nvimtree")
require("config.blankline")
require("config.virtcol")
require("config.telescope")
require("config.harpoon")
