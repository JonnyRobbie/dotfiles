#!/usr/bin/env python3

import argparse
import os
import shutil
from functools import reduce
from pathlib import Path

from simple_term_menu import TerminalMenu


def walk_paths(paths: list[str]) -> list[Path]:
    """Get a list of all files in the supplied arguments."""
    return reduce(
        lambda x, y: x + y,
        [
            reduce(
                lambda x, y: x + [Path(y[0]) / filename for filename in y[2]],
                os.walk(path),
                [],
            )
            if Path(path).is_dir()
            else [Path(path)]
            for path in paths
        ],
    )


def copy_dotfiles(filenames: list[Path], *, dry: bool = True) -> None:
    """Copy a dotfile from user home to repo links."""
    for filename in filenames:
        common_path = Path(*filename.parts[: len(Path.home().parts)])
        if common_path != Path.home():
            msg = f"{filename} is not in the users home directory"
            raise ValueError(msg)
        repo_filename = (
            Path.cwd() / "links" / Path(*filename.parts[len(Path.home().parts) :])
        )
        print(f"{filename} -> {repo_filename}")
        if not dry:
            shutil.copy(filename, repo_filename)


def link_dotfiles(filenames: list[Path], *, dry: bool = True) -> None:
    """Link dotfiles from user home to repo."""
    for repo_filename in filenames:
        home_filename = Path.home() / Path(
            *repo_filename.parts[len(Path.cwd().parts) + 1 :],
        )
        if home_filename.exists() and not dry:
            bkp_name = home_filename.with_name(home_filename.name + "~")
            home_filename.rename(bkp_name)
        print(f"(link) {home_filename} -> {repo_filename}")
        if not dry:
            home_filename.symlink_to(repo_filename)


def add_dots(arg_paths: list[str], *, dry: bool) -> None:
    """Present the UI for adding dotfiles."""
    files = walk_paths(arg_paths)
    entries = [str(f) for f in files]
    menu_add = TerminalMenu(
        entries,
        title="Confirm dotfiles to add to the repo:",
        multi_select=True,
        show_multi_select_hint=True,
        preselected_entries=entries,
    )
    selected = menu_add.show()
    selected = (selected,) if isinstance(selected, int) else selected
    selected = () if selected is None else selected
    selected_paths = [files[i] for i in selected]
    copy_dotfiles(selected_paths, dry=dry)


def restore_dots(*, dry: bool) -> None:
    """Present the UI for restoring dotfiles."""
    links_path = Path.cwd() / "links"
    files = walk_paths([str(links_path)])
    entries = [str(Path(*f.parts[len(links_path.parts) :])) for f in files]
    menu_restore = TerminalMenu(
        entries,
        title="Select dotfiles to be restored from the repo (linked to):",
        multi_select=True,
        show_multi_select_hint=True,
    )
    selected = menu_restore.show()
    selected = (selected,) if isinstance(selected, int) else selected
    selected = () if selected is None else selected
    selected_paths = [files[i] for i in selected]
    link_dotfiles(selected_paths, dry=dry)


def main(args: argparse.Namespace) -> None:
    """Entry point."""
    if len(args.add) > 0:
        add_dots(args.add, dry=args.dry)
    if args.restore:
        restore_dots(dry=args.dry)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Add or restore dotfiles")
    parser.add_argument("add", nargs="*", help="Add a dotfile(s) to the repo")
    parser.add_argument(
        "-r",
        "--restore",
        action="store_false",
        help="Restore dotfiles from the repo",
    )
    parser.add_argument("-d", "--dry", help="Dry run", action="store_true")
    args = parser.parse_args()
    main(args)
